/**
 * URL Base para a API do Passaporte.
 * Utilizado para consulta das Unidades.
 */
const passaporteURLBase = 'https://ws.tcm.go.gov.br/api/rest/passaporteService/'

/**
 * URL Base para a API do Colare.
 * Utilizado na consulta das Legislações, Cargos e Verbas.
 */
const consultaURLBase = 'https://ws.tcm.go.gov.br/api/rest/colareService/'

/**
 * URL para obter PDf de Homologação.
 */
const pdfHomologacaoURL = 'https://virtual.tcm.go.gov.br/envio-manual/api/envio/pdf/homologacao/'

const dados = {
    unidades: [],
    legislacoes: [],
    cargos: [],
    verbas: []
}

//Municípios

/**
 * Obtém os dados dos Municípios
 * do arquivo JSON local.
 */
function obtemDadosMunicipios() {
    const municipiosURL = 'json/municipios.json'
    return axios.get(municipiosURL)
}

/**
 * Preenche os dados dos municípios.
 * Utilizado ao inicializar.
 * Chama os métodos para obtenção de dados e
 * exibição visual dos dados.
 */
function preencheMunicipios() {
    obtemDadosMunicipios()
    .then(response => {
        exibeMunicipios(response.data)
    })
}

/**
 * Cria os elementos visuais para exibição de
 * municípios.
 * @param municipios Municípios para preencher select.
 */
function exibeMunicipios(municipios) {
    const municipioSelect = document.getElementById('municipios')
    
    municipios.map(item => {
        const option = document.createElement('option')
        option.value = item.codigoIBGE
        option.innerHTML = item.municipio
        municipioSelect.appendChild(option)
    })

}


// Tabelas

/**
 * Obtém os dados das Tabelas
 * do arquivo JSON local.
 */
function obtemDadosTabelas() {
    const tabelasURL = 'json/tabelas.json'
    return axios.get(tabelasURL)
}

/**
 * Preenche os dados das tabelas.
 * Utilizado ao inicializar.
 * Chama os métodos para obtenção de dados e
 * exibição visual dos dados.
 */
function preencheTabelas() {
    obtemDadosTabelas()
    .then(response => {
        exibeTabelas(response.data)
    })
}

/**
 * Cria os elementos visuais para exibição de
 * tabelas.
 * @param tabelas Tabelas para preencher select.
 */
function exibeTabelas(tabelas) {
    const tabelaSelect = document.getElementById('tabelas')
    
    tabelas.map(item => {
        const option = document.createElement('option')
        option.value = item.valor
        option.innerHTML = item.nome
        tabelaSelect.appendChild(option)
    })

}


// Obtenção de dados preenchidos

/**
 * Obtém dados do município selecionado.
 */
function obtemMunicipioSelecionado() {
    const municipioSelecionado = document.getElementById('municipios')
    return municipioSelecionado.value
}

/**
 * Obtém dados da tabela selecionada.
 */
function obtemTabelaSelecionada() {
    const tabelaSelecionada = document.getElementById('tabelas')
    return tabelaSelecionada.value
}

/**
 * Realiza pesquisa conforme tabela selecionada.
 * Valida se o Município e Tabela foram selecionados.
 */
function pesquisar() {
    const municipioSelecionado = obtemMunicipioSelecionado()
    const tabelaSelecionada = obtemTabelaSelecionada()

    if (municipioSelecionado == "") {
        alert('Preencher o município...')
    } else if(tabelaSelecionada == "") {
        alert('Selecionar a tabela...')
    } else {
        preencherDados(municipioSelecionado, tabelaSelecionada)
    }

}

/**
 * Esconde as tabelas de dados na tela.
 */
function escondeTabelas() {
    const sessoes = document.getElementsByTagName('section')
    for (let index = 0; index < sessoes.length; index++) {
        const sessao = sessoes[index];
        if (sessao.id != "controles") {
            sessao.style.display = 'none'
        }
    }
}

/**
 * Preenche os dados conforme município e tabela selecionadas.
 * @param municipio Município a ser pesquisado.
 * @param tabela Tabela a ser pesquisada.
 */
function preencherDados(municipio, tabela) {

    escondeTabelas()

    switch (tabela) {
        case 'unidadesGestoras':
            preencherUnidadeGestora(municipio)
            break;
        case 'legislacoes':
            preencherLegislacoes(municipio)
            break;
        case 'cargos':
            preencherCargos(municipio)
            break;
        case 'verbas':
            preencherVerbas(municipio)
            break;
    }

}

// Unidades Gestoras

/**
 * Obtém os dados das Unidades Gestoras conforme município.
 * @param municipio Município a ser pesquisado.
 */
function obtemDadosUnidadesGestoras(municipio) {
    const url = `${passaporteURLBase}${municipio}/unidadesGestoras`
    return axios.get(url)
}

/**
 * Preenche a tabela de Unidade Gestora consultando 
 * os dados e criando a tabela.
 * @param municipio Municipio a ser pesquisado.
 */
function preencherUnidadeGestora(municipio) {
    criaModalLoading()
    window.location = '#modal'
    obtemDadosUnidadesGestoras(municipio)
    .then(response => {
        dados.unidades = response.data
        criaTabelaUnidadeGestora(dados.unidades)
        window.location = '#'
    })
}

/**
 * Cria as tabelas de Unidade Gestora.
 * @param unidades Dados das Unidades a serem preenchidas na tabela.
 */
function criaTabelaUnidadeGestora(unidades) {
    
    const corpoTabela = document.getElementById('tblUnidadesGestoras').tBodies[0]

    unidades.map((item, indice) => {
 
        const linha = document.createElement('tr')

        const tdID = document.createElement('td')
        const idLink = document.createElement('a')
        idLink.href = '#modal'
        idLink.addEventListener('click', () => {
            criaModalUnidadeGestora(indice)
        })
        idLink.innerText = item.id
        tdID.appendChild(idLink)
        linha.appendChild(tdID)

        const tdNome = document.createElement('td')
        tdNome.innerText = item.nome
        linha.appendChild(tdNome)

        const tdCNPJ = document.createElement('td')
        tdCNPJ.innerText = item.cnpj
        linha.appendChild(tdCNPJ)

        const tdTipo = document.createElement('td')
        tdTipo.innerText = item.tipo
        linha.appendChild(tdTipo)

        const tdDataLei = document.createElement('td')
        tdDataLei.innerText = item.dataLeiCriacao
        linha.appendChild(tdDataLei)

        const tdDataCadastro = document.createElement('td')
        tdDataCadastro.innerText = item.dataCadastro
        linha.appendChild(tdDataCadastro)

        const tdStatus = document.createElement('td')
        tdStatus.innerText = item.status
        linha.appendChild(tdStatus)

        corpoTabela.appendChild(linha)
    })

    document.getElementById('unidadesGestoras').style.display = 'block'
}

/**
 * Cria o Modal com detalhes da Unidade Gestora.
 * @param indice Índice da lista de Unidades Gestoras.
 */
function criaModalUnidadeGestora(indice) {
    const unidade = dados.unidades[indice]

    const dadosModal = document.getElementById('dadosModal')
    dadosModal.innerHTML = ''

    const linkFechar = document.createElement('a')
    linkFechar.href= '#'
    linkFechar.className = 'fechar'
    linkFechar.innerText = 'X'
    dadosModal.appendChild(linkFechar)

    const tituloModal = document.createElement('h2')
    tituloModal.innerText = 'Detalhes Unidade Gestora'
    dadosModal.appendChild(tituloModal)

    const pId = document.createElement('p')
    const bId = document.createElement('b')
    bId.innerText = 'ID: '
    const spanId = document.createElement('span')
    spanId.innerText = unidade.id
    pId.appendChild(bId)
    pId.appendChild(spanId)
    dadosModal.appendChild(pId)

    const pNome = document.createElement('p')
    const bNome = document.createElement('b')
    bNome.innerText = 'Nome: '
    const spanNome = document.createElement('span')
    spanNome.innerText = unidade.nome
    pNome.appendChild(bNome)
    pNome.appendChild(spanNome)
    dadosModal.appendChild(pNome)

    const pCNPJ = document.createElement('p')
    const bCNPJ = document.createElement('b')
    bCNPJ.innerText = 'CNPJ: '
    const spanCNPJ = document.createElement('span')
    spanCNPJ.innerText = unidade.cnpj
    pCNPJ.appendChild(bCNPJ)
    pCNPJ.appendChild(spanCNPJ)
    dadosModal.appendChild(pCNPJ)

    const pTipo = document.createElement('p')
    const bTipo = document.createElement('b')
    bTipo.innerText = 'Tipo: '
    const spanTipo = document.createElement('span')
    spanTipo.innerText = unidade.tipo
    pTipo.appendChild(bTipo)
    pTipo.appendChild(spanTipo)
    dadosModal.appendChild(pTipo)

    const pDataLei = document.createElement('p')
    const bDataLei = document.createElement('b')
    bDataLei.innerText = 'Data da Lei de Criação: '
    const spanDataLei = document.createElement('span')
    spanDataLei.innerText = unidade.dataLeiCriacao
    pDataLei.appendChild(bDataLei)
    pDataLei.appendChild(spanDataLei)
    dadosModal.appendChild(pDataLei)

    const pDataCadastro = document.createElement('p')
    const bDataCadastro = document.createElement('b')
    bDataCadastro.innerText = 'Data de Cadastro: '
    const spanDataCadastro = document.createElement('span')
    spanDataCadastro.innerText = unidade.dataCadastro
    pDataCadastro.appendChild(bDataCadastro)
    pDataCadastro.appendChild(spanDataCadastro)
    dadosModal.appendChild(pDataCadastro)

    const pStatus = document.createElement('p')
    const bStatus = document.createElement('b')
    bStatus.innerText = 'Status: '
    const spanStatus = document.createElement('span')
    spanStatus.innerText = unidade.nome
    pStatus.appendChild(bStatus)
    pStatus.appendChild(spanStatus)
    dadosModal.appendChild(pStatus)
    
}

// Legislações

/**
 * Obtém dados das Legislações conforme município.
 * @param municipio Município a ser pesquisado.
 */
function obtemDadosLegislacoes(municipio) {
    const url = `${consultaURLBase}${municipio}/legislacoes`
    return axios.get(url)
}

/**
 * Preenche a tabela de Legislações consultando
 * os dados e criando a tabela.
 * @param municipio Município a ser pesquisado.
 */
function preencherLegislacoes(municipio) {
    criaModalLoading()
    window.location = '#modal'
    obtemDadosLegislacoes(municipio)
    .then(response => {
        dados.legislacoes = response.data
        criaTabelaLegislacoes(dados.legislacoes)
        window.location = '#'
    })
}

/**
 * Cria as tabelas de Legislações.
 * @param legislacoes Legislações a serem preenchidas na tabela.
 */
function criaTabelaLegislacoes(legislacoes) {
    const corpoTabela = document.getElementById('tblLegislacoes').tBodies[0]

    legislacoes.map((item, indice) => {
        const linha = document.createElement('tr')

        const tdID = document.createElement('td')
        const idLink = document.createElement('a')
        idLink.href = '#modal'
        idLink.addEventListener('click', () => {
            criaModalLegislacoes(indice)
        })
        idLink.innerText = item.idEnvioColare
        tdID.appendChild(idLink)
        linha.appendChild(tdID)

        const tdTipoNorma = document.createElement('td')
        tdTipoNorma.innerText = item.tipoNorma
        linha.appendChild(tdTipoNorma)

        const tdNumero = document.createElement('td')
        tdNumero.innerText = item.numeroNorma
        linha.appendChild(tdNumero)

        const tdAno = document.createElement('td')
        tdAno.innerText = item.anoNorma
        linha.appendChild(tdAno)

        const tdDataEnvio = document.createElement('td')
        tdDataEnvio.innerText = item.dataEnvio
        linha.appendChild(tdDataEnvio)

        const tdUnidadeGestora = document.createElement('td')
        tdUnidadeGestora.innerText = item.unidadeGestoraId + ' - ' + item.unidadeGestora
        linha.appendChild(tdUnidadeGestora)

        const tdSituacao = document.createElement('td')
        tdSituacao.innerText = item.situacaoEnvio
        linha.appendChild(tdSituacao)

        const tdPDF = document.createElement('td')
        const linkPDF = document.createElement('a')
        linkPDF.href = item.arquivoPrincipalNorma
        linkPDF.target = '_blank'
        linkPDF.innerText = 'PDF'
        tdPDF.appendChild(linkPDF)
        linha.appendChild(tdPDF)

        const tdPDFRecibo = document.createElement('td')
        const linkPDFRecibo = document.createElement('a')
        linkPDFRecibo.href = `${pdfHomologacaoURL}${item.idEnvioColare}`
        linkPDFRecibo.target = '_blank'
        linkPDFRecibo.innerText = 'PDF do Recibo'
        tdPDFRecibo.appendChild(linkPDFRecibo)
        linha.appendChild(tdPDFRecibo)

        corpoTabela.appendChild(linha)
    })

    document.getElementById('legislacoes').style.display = 'block'
}

/**
 * Cria o modal com detalhes da Legislação.
 * @param indice Índice da lista de Legislações.
 */
function criaModalLegislacoes(indice) {
    const legislacao = dados.legislacoes[indice]

    const dadosModal = document.getElementById('dadosModal')
    dadosModal.innerHTML = ''

    const linkFechar = document.createElement('a')
    linkFechar.href= '#'
    linkFechar.className = 'fechar'
    linkFechar.innerText = 'X'
    dadosModal.appendChild(linkFechar)

    const tituloModal = document.createElement('h2')
    tituloModal.innerText = 'Detalhes Legislação'
    dadosModal.appendChild(tituloModal)

    const pId = document.createElement('p')
    const bId = document.createElement('b')
    bId.innerText = 'ID: '
    const spanId = document.createElement('span')
    spanId.innerText = legislacao.idEnvioColare
    pId.appendChild(bId)
    pId.appendChild(spanId)
    dadosModal.appendChild(pId)

    const pTipoNorma = document.createElement('p')
    const bTipoNorma = document.createElement('b')
    bTipoNorma.innerText = 'Tipo da Norma: '
    const spanTipoNorma = document.createElement('span')
    spanTipoNorma.innerText = legislacao.tipoNorma
    pTipoNorma.appendChild(bTipoNorma)
    pTipoNorma.appendChild(spanTipoNorma)
    dadosModal.appendChild(pTipoNorma)

    const pNumeroNorma = document.createElement('p')
    const bNumeroNorma = document.createElement('b')
    bNumeroNorma.innerText = 'Número da Norma: '
    const spanNumeroNorma = document.createElement('span')
    spanNumeroNorma.innerText = legislacao.numeroNorma
    pNumeroNorma.appendChild(bNumeroNorma)
    pNumeroNorma.appendChild(spanNumeroNorma)
    dadosModal.appendChild(pNumeroNorma)

    const pAnoNorma = document.createElement('p')
    const bAnoNorma = document.createElement('b')
    bAnoNorma.innerText = 'Ano da Norma: '
    const spanAnoNorma = document.createElement('span')
    spanAnoNorma.innerText = legislacao.anoNorma
    pAnoNorma.appendChild(bAnoNorma)
    pAnoNorma.appendChild(spanAnoNorma)
    dadosModal.appendChild(pAnoNorma)

    const pDataEnvio = document.createElement('p')
    const bDataEnvio = document.createElement('b')
    bDataEnvio.innerText = 'Data de Envio: '
    const spanDataEnvio = document.createElement('span')
    spanDataEnvio.innerText = legislacao.dataEnvio
    pDataEnvio.appendChild(bDataEnvio)
    pDataEnvio.appendChild(spanDataEnvio)
    dadosModal.appendChild(pDataEnvio)

    const pUnidadeGestora = document.createElement('p')
    const bUnidadeGestora = document.createElement('b')
    bUnidadeGestora.innerText = 'Unidade Gestora: '
    const spanUnidadeGestora = document.createElement('span')
    spanUnidadeGestora.innerText = legislacao.unidadeGestoraId + ' - ' + legislacao.unidadeGestora
    pUnidadeGestora.appendChild(bUnidadeGestora)
    pUnidadeGestora.appendChild(spanUnidadeGestora)
    dadosModal.appendChild(pUnidadeGestora)

    const pStatus = document.createElement('p')
    const bStatus = document.createElement('b')
    bStatus.innerText = 'Status: '
    const spanStatus = document.createElement('span')
    spanStatus.innerText = legislacao.situacaoEnvio
    pStatus.appendChild(bStatus)
    pStatus.appendChild(spanStatus)
    dadosModal.appendChild(pStatus)
    
}

// Cargos

/**
 * Obtém dados dos cargos conforme município.
 * @param municipio Município a ser pesquisado.
 */
function obtemDadosCargos(municipio) {
    const url = `${consultaURLBase}${municipio}/cargos`
    return axios.get(url)
}

/**
 * Preenche a tabela de Cargos consultando
 * os dados e criando a tabela.
 * @param municipio Município a ser pesquisado.
 */
function preencherCargos(municipio) {
    criaModalLoading()
    window.location = '#modal'
    obtemDadosCargos(municipio)
    .then(response => {
        dados.cargos = response.data
        criaTabelaCargos(dados.cargos)
        window.location = '#'
    })
}

/**
 * Cria as tabelas de Cargos.
 * @param cargos Cargos a serem preenchidos na tabela
 */
function criaTabelaCargos(cargos) {
    const corpoTabela = document.getElementById('tblCargos').tBodies[0]

    cargos.map((item, indice) => {
        const linha = document.createElement('tr')

        const tdID = document.createElement('td')
        const idLink = document.createElement('a')
        idLink.href = '#modal'
        idLink.addEventListener('click', () => {
            criaModalCargos(indice)
        })
        idLink.innerText = item.idEnvioColare
        tdID.appendChild(idLink)
        linha.appendChild(tdID)

        const tdNome = document.createElement('td')
        tdNome.innerText = item.nomeCargo
        linha.appendChild(tdNome)

        const tdDataEnvio = document.createElement('td')
        tdDataEnvio.innerText = item.dataEnvio
        linha.appendChild(tdDataEnvio)

        const tdUnidadeGestora = document.createElement('td')
        tdUnidadeGestora.innerText = item.unidadeGestoraId + ' - ' + item.unidadeGestora
        linha.appendChild(tdUnidadeGestora)

        const tdCargaHoraria = document.createElement('td')
        tdCargaHoraria.innerText = item.cargaHorariaSemanal + 'hs/semana'
        linha.appendChild(tdCargaHoraria)

        const tdLegislacao = document.createElement('td')
        tdLegislacao.innerText = item.idPessoalLegislacao
        linha.appendChild(tdLegislacao)

        const tdSituacao = document.createElement('td')
        tdSituacao.innerText = item.situacaoEnvio
        linha.appendChild(tdSituacao)

        const tdPDFRecibo = document.createElement('td')
        const linkPDFRecibo = document.createElement('a')
        linkPDFRecibo.href = `${pdfHomologacaoURL}${item.idEnvioColare}`
        linkPDFRecibo.target = '_blank'
        linkPDFRecibo.innerText = 'PDF do Recibo'
        tdPDFRecibo.appendChild(linkPDFRecibo)
        linha.appendChild(tdPDFRecibo)

        corpoTabela.appendChild(linha)
    })

    document.getElementById('cargos').style.display = 'block'
}

/**
 * Cria o modal com detalhes do Cargo.
 * @param indice Índice da lista de Cargos.
 */
function criaModalCargos(indice) {
    const cargo = dados.cargos[indice]

    const dadosModal = document.getElementById('dadosModal')
    dadosModal.innerHTML = ''

    const linkFechar = document.createElement('a')
    linkFechar.href= '#'
    linkFechar.className = 'fechar'
    linkFechar.innerText = 'X'
    dadosModal.appendChild(linkFechar)

    const tituloModal = document.createElement('h2')
    tituloModal.innerText = 'Detalhes Cargo'
    dadosModal.appendChild(tituloModal)

    const pId = document.createElement('p')
    const bId = document.createElement('b')
    bId.innerText = 'ID: '
    const spanId = document.createElement('span')
    spanId.innerText = cargo.idEnvioColare
    pId.appendChild(bId)
    pId.appendChild(spanId)
    dadosModal.appendChild(pId)

    const pNomeCargo = document.createElement('p')
    const bNomeCargo = document.createElement('b')
    bNomeCargo.innerText = 'Nome do Cargo: '
    const spanNomeCargo = document.createElement('span')
    spanNomeCargo.innerText = cargo.nomeCargo
    pNomeCargo.appendChild(bNomeCargo)
    pNomeCargo.appendChild(spanNomeCargo)
    dadosModal.appendChild(pNomeCargo)

    const pDataEnvio = document.createElement('p')
    const bDataEnvio = document.createElement('b')
    bDataEnvio.innerText = 'Data de Envio: '
    const spanDataEnvio = document.createElement('span')
    spanDataEnvio.innerText = cargo.dataEnvio
    pDataEnvio.appendChild(bDataEnvio)
    pDataEnvio.appendChild(spanDataEnvio)
    dadosModal.appendChild(pDataEnvio)

    const pUnidadeGestora = document.createElement('p')
    const bUnidadeGestora = document.createElement('b')
    bUnidadeGestora.innerText = 'Unidade Gestora: '
    const spanUnidadeGestora = document.createElement('span')
    spanUnidadeGestora.innerText = cargo.unidadeGestoraId + ' - ' + cargo.unidadeGestora
    pUnidadeGestora.appendChild(bUnidadeGestora)
    pUnidadeGestora.appendChild(spanUnidadeGestora)
    dadosModal.appendChild(pUnidadeGestora)

    const pRegimeJuridico = document.createElement('p')
    const bRegimeJuridico = document.createElement('b')
    bRegimeJuridico.innerText = 'Regime Jurídico: '
    const spanRegimeJuridico = document.createElement('span')
    spanRegimeJuridico.innerText = cargo.regimeJuridico
    pRegimeJuridico.appendChild(bRegimeJuridico)
    pRegimeJuridico.appendChild(spanRegimeJuridico)
    dadosModal.appendChild(pRegimeJuridico)

    const pQuadroPessoal = document.createElement('p')
    const bQuadroPessoal = document.createElement('b')
    bQuadroPessoal.innerText = 'Quadro Pessoal: '
    const spanQuadroPessoal = document.createElement('span')
    spanQuadroPessoal.innerText = cargo.quadroPessoal
    pQuadroPessoal.appendChild(bQuadroPessoal)
    pQuadroPessoal.appendChild(spanQuadroPessoal)
    dadosModal.appendChild(pQuadroPessoal)

    const pQuantitativoCargo = document.createElement('p')
    const bQuantitativoCargo = document.createElement('b')
    bQuantitativoCargo.innerText = 'Quantidade de Vagas: '
    const spanQuantitativoCargo = document.createElement('span')
    spanQuantitativoCargo.innerText = cargo.quantitativoCargo
    pQuantitativoCargo.appendChild(bQuantitativoCargo)
    pQuantitativoCargo.appendChild(spanQuantitativoCargo)
    dadosModal.appendChild(pQuantitativoCargo)

    const pCBO = document.createElement('p')
    const bCBO = document.createElement('b')
    bCBO.innerText = 'CBO: '
    const spanCBO = document.createElement('span')
    spanCBO.innerText = cargo.cbo
    pCBO.appendChild(bCBO)
    pCBO.appendChild(spanCBO)
    dadosModal.appendChild(pCBO)

    const pEscolaridade = document.createElement('p')
    const bEscolaridade = document.createElement('b')
    bEscolaridade.innerText = 'Escolaridade Mínima: '
    const spanEscolaridade = document.createElement('span')
    spanEscolaridade.innerText = cargo.escolaridadeMinima
    pEscolaridade.appendChild(bEscolaridade)
    pEscolaridade.appendChild(spanEscolaridade)
    dadosModal.appendChild(pEscolaridade)

    const pCargaHoraria = document.createElement('p')
    const bCargaHoraria = document.createElement('b')
    bCargaHoraria.innerText = 'Carga Horária: '
    const spanCargaHoraria = document.createElement('span')
    spanCargaHoraria.innerText = cargo.cargaHorariaSemanal + 'hs/semana'
    pCargaHoraria.appendChild(bCargaHoraria)
    pCargaHoraria.appendChild(spanCargaHoraria)
    dadosModal.appendChild(pCargaHoraria)

    const pLegislacao = document.createElement('p')
    const bLegislacao = document.createElement('b')
    bLegislacao.innerText = 'Legislação: '
    const spanLegislacao = document.createElement('span')
    spanLegislacao.innerText = cargo.idPessoalLegislacao
    pLegislacao.appendChild(bLegislacao)
    pLegislacao.appendChild(spanLegislacao)
    dadosModal.appendChild(pLegislacao)

    const pSituacao = document.createElement('p')
    const bSituacao = document.createElement('b')
    bSituacao.innerText = 'Situação: '
    const spanSituacao = document.createElement('span')
    spanSituacao.innerText = cargo.situacaoEnvio
    pSituacao.appendChild(bSituacao)
    pSituacao.appendChild(spanSituacao)
    dadosModal.appendChild(pSituacao)
}

// Verbas

/**
 * Obtém dados das verbas conforme município.
 * @param municipio Município a ser pesquisado.
 */
function obtemDadosVerbas(municipio) {
    const url = `${consultaURLBase}${municipio}/verbas`
    return axios.get(url)
}

/**
 * Preenche a tabela de Verbas consultando
 * os dados e criando a tabela.
 * @param municipio Município a ser pesquisado.
 */
function preencherVerbas(municipio) {
    criaModalLoading()
    window.location = '#modal'
    obtemDadosVerbas(municipio)
    .then(response => {
        dados.verbas = response.data
        criaTabelaVerbas(dados.verbas)
        window.location = '#'
    })
}

/**
 * Cria as tabelas de Verbas.
 * @param verbas Verbas a serem preenchidas na tabela
 */
function criaTabelaVerbas(verbas) {
    const corpoTabela = document.getElementById('tblVerbas').tBodies[0]

    verbas.map((item, indice) => {
        const linha = document.createElement('tr')

        const tdID = document.createElement('td')
        const idLink = document.createElement('a')
        idLink.href = '#modal'
        idLink.addEventListener('click', () => {
            criaModalVerbas(indice)
        })
        idLink.innerText = item.idEnvioColare
        tdID.appendChild(idLink)
        linha.appendChild(tdID)

        const tdNome = document.createElement('td')
        tdNome.innerText = item.nomeVerba
        linha.appendChild(tdNome)

        const tdDataEnvio = document.createElement('td')
        tdDataEnvio.innerText = item.dataEnvio
        linha.appendChild(tdDataEnvio)

        const tdUnidadeGestora = document.createElement('td')
        tdUnidadeGestora.innerText = item.unidadeGestoraRepresentanteId + ' - ' + item.unidadeGestoraRepresentante
        linha.appendChild(tdUnidadeGestora)

        const tdLegislacao = document.createElement('td')
        tdLegislacao.innerText = item.idPessoalLegislacao
        linha.appendChild(tdLegislacao)

        const tdSituacao = document.createElement('td')
        tdSituacao.innerText = item.situacaoEnvio
        linha.appendChild(tdSituacao)

        const tdPDFRecibo = document.createElement('td')
        const linkPDFRecibo = document.createElement('a')
        linkPDFRecibo.href = `${pdfHomologacaoURL}${item.idEnvioColare}`
        linkPDFRecibo.target = '_blank'
        linkPDFRecibo.innerText = 'PDF do Recibo'
        tdPDFRecibo.appendChild(linkPDFRecibo)
        linha.appendChild(tdPDFRecibo)

        corpoTabela.appendChild(linha)
    })

    document.getElementById('verbas').style.display = 'block'
}

/**
 * Cria o modal com detalhes do Verbas.
 * @param indice Índice da lista de Verbas.
 */
function criaModalVerbas(indice) {
    const verba = dados.verbas[indice]

    const dadosModal = document.getElementById('dadosModal')
    dadosModal.innerHTML = ''

    const linkFechar = document.createElement('a')
    linkFechar.href= '#'
    linkFechar.className = 'fechar'
    linkFechar.innerText = 'X'
    dadosModal.appendChild(linkFechar)

    const tituloModal = document.createElement('h2')
    tituloModal.innerText = 'Detalhes Verba'
    dadosModal.appendChild(tituloModal)

    const pId = document.createElement('p')
    const bId = document.createElement('b')
    bId.innerText = 'ID: '
    const spanId = document.createElement('span')
    spanId.innerText = verba.idEnvioColare
    pId.appendChild(bId)
    pId.appendChild(spanId)
    dadosModal.appendChild(pId)

    const pNomeVerba = document.createElement('p')
    const bNomeVerba = document.createElement('b')
    bNomeVerba.innerText = 'Nome da Verba: '
    const spanNomeVerba = document.createElement('span')
    spanNomeVerba.innerText = verba.nomeVerba
    pNomeVerba.appendChild(bNomeVerba)
    pNomeVerba.appendChild(spanNomeVerba)
    dadosModal.appendChild(pNomeVerba)

    const pDataEnvio = document.createElement('p')
    const bDataEnvio = document.createElement('b')
    bDataEnvio.innerText = 'Data de Envio: '
    const spanDataEnvio = document.createElement('span')
    spanDataEnvio.innerText = verba.dataEnvio
    pDataEnvio.appendChild(bDataEnvio)
    pDataEnvio.appendChild(spanDataEnvio)
    dadosModal.appendChild(pDataEnvio)

    const pUnidadeGestora = document.createElement('p')
    const bUnidadeGestora = document.createElement('b')
    bUnidadeGestora.innerText = 'Unidade Gestora: '
    const spanUnidadeGestora = document.createElement('span')
    spanUnidadeGestora.innerText = verba.unidadeGestoraRepresentanteId + ' - ' + verba.unidadeGestoraRepresentante
    pUnidadeGestora.appendChild(bUnidadeGestora)
    pUnidadeGestora.appendChild(spanUnidadeGestora)
    dadosModal.appendChild(pUnidadeGestora)

    const pTipoVerba = document.createElement('p')
    const bTipoVerba = document.createElement('b')
    bTipoVerba.innerText = 'Tipo Verba: '
    const spanTipoVerba = document.createElement('span')
    spanTipoVerba.innerText = verba.tipoVerba
    pTipoVerba.appendChild(bTipoVerba)
    pTipoVerba.appendChild(spanTipoVerba)
    dadosModal.appendChild(pTipoVerba)

    const pMunicipio = document.createElement('p')
    const bMunicipio = document.createElement('b')
    bMunicipio.innerText = 'Município: '
    const spanMunicipio = document.createElement('span')
    spanMunicipio.innerText = verba.municipio
    pMunicipio.appendChild(bMunicipio)
    pMunicipio.appendChild(spanMunicipio)
    dadosModal.appendChild(pMunicipio)

    const pVerbaTransitoria = document.createElement('p')
    const bVerbaTransitoria = document.createElement('b')
    bVerbaTransitoria.innerText = 'Verba Transitória: '
    const spanVerbaTransitoria = document.createElement('span')
    spanVerbaTransitoria.innerText = verba.verbaTransitoria ? 'Sim' : 'Não'
    pVerbaTransitoria.appendChild(bVerbaTransitoria)
    pVerbaTransitoria.appendChild(spanVerbaTransitoria)
    dadosModal.appendChild(pVerbaTransitoria)

    const pQuadroPessoal = document.createElement('p')
    const bQuadroPessoal = document.createElement('b')
    bQuadroPessoal.innerText = 'Quadro Pessoal: '
    const spanQuadroPessoal = document.createElement('span')
    spanQuadroPessoal.innerText = verba.quadroPessoal
    pQuadroPessoal.appendChild(bQuadroPessoal)
    pQuadroPessoal.appendChild(spanQuadroPessoal)
    dadosModal.appendChild(pQuadroPessoal)

    const pRepresentante = document.createElement('p')
    const bRepresentante = document.createElement('b')
    bRepresentante.innerText = 'Representante: '
    const spanRepresentante = document.createElement('span')
    spanRepresentante.innerText = verba.representante
    pRepresentante.appendChild(bRepresentante)
    pRepresentante.appendChild(spanRepresentante)
    dadosModal.appendChild(pRepresentante)

    const pDataInicio = document.createElement('p')
    const bDataInicio = document.createElement('b')
    bDataInicio.innerText = 'Data Inicio: '
    const spanDataInicio = document.createElement('span')
    spanDataInicio.innerText = verba.dataInicio
    pDataInicio.appendChild(bDataInicio)
    pDataInicio.appendChild(spanDataInicio)
    dadosModal.appendChild(pDataInicio)

    const pLegislacao = document.createElement('p')
    const bLegislacao = document.createElement('b')
    bLegislacao.innerText = 'Legislação: '
    const spanLegislacao = document.createElement('span')
    spanLegislacao.innerText = verba.idPessoalLegislacao
    pLegislacao.appendChild(bLegislacao)
    pLegislacao.appendChild(spanLegislacao)
    dadosModal.appendChild(pLegislacao)

    const pSituacao = document.createElement('p')
    const bSituacao = document.createElement('b')
    bSituacao.innerText = 'Situação: '
    const spanSituacao = document.createElement('span')
    spanSituacao.innerText = verba.situacaoEnvio
    pSituacao.appendChild(bSituacao)
    pSituacao.appendChild(spanSituacao)
    dadosModal.appendChild(pSituacao)
}

/**
 * Cria Modal para Carregamento
 */
function criaModalLoading() {
    const dadosModal = document.getElementById('dadosModal')
    dadosModal.innerHTML = ''

    const loading = document.createElement('img')
    loading.src = 'img/loading.gif'

    dadosModal.appendChild(loading)
}

function inicia() {

    escondeTabelas()

    preencheMunicipios()

    preencheTabelas()

    const btnPesquisar = document.getElementById('btnPesquisar')
    btnPesquisar.addEventListener('click', pesquisar)

}

inicia()